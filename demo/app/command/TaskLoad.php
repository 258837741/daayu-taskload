<?php
declare (strict_types = 1);

namespace app\command;

use daayu\taskload\TaskLoadCommand;

class TaskLoad extends TaskLoadCommand
{
    protected $connection = 'zhuna_cn';
}
