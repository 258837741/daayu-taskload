<?php
declare (strict_types = 1);

namespace daayu\taskload;

/**
 * 配置类
 */
class TaskLoadConfig
{
    const DEFAULT_CONNECTION = 'mysql';
    
    const LOG_PATH = '/var/log/autotask/';
    const CRON_PATH = '/var/spool/cron/root';
    const SHELL_PATH = 'vendor/daayu/taskload/src/shell/';
    const TAG_START = '###AutoTaskStart###';
    const TAG_END = '###AutoTaskEnd###';

    const TABLE_TASK_PLAN = 'zn_task_plan';
    const TABLE_TASK_PROJECT = 'zn_task_project';
    const TABLE_TASK_SERVER = 'zn_task_server';
    const TABLE_TASK_SERVER_RELATE = 'zn_task_server_relate';
    const TABLE_TASK_SERVER_PROJECT = 'zn_task_server_project';
}
