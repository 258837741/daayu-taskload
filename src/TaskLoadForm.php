<?php
declare (strict_types = 1);

namespace daayu\taskload;

/**
 * 表单
 */
class TaskLoadForm
{
    /**
     * 正则验证url地址
     */
    public static function checkUrl($text)
    {
        if(empty($text) || !preg_match("/^(http|https):\/\/.*$/i", $text)){
            return false;
        }else{
            return true;
        }
    }

    /**
     * 正则验证think命令
     */
    public static function checkCommand($text)
    {
        if(empty($text) || !preg_match("/^[-=\sa-zA-Z0-9\.'_\x80-\xff]+$/", $text)){
            return false;
        }else{
            return true;
        }
    }

    /**
     * 正则验证日志文件名
     */
    public static function checkLogFile($text)
    {
        if(empty($text) || !preg_match("/^[a-zA-Z0-9_]+$/", $text)){
            return false;
        }else{
            return true;
        }
    }

    /**
     * 正则验证执行频率规则
     */
    public static function checkTimeRule($text)
    {
        if(strpos($text, '|')){
            $items = explode('|', $text);
        }else{
            $items = explode(' ', $text);
        }
        if(count($items) != 5) return false;
        $pattern = "/^(\*|\*\/[0-9]+|^[-0-9,]+$|[-0-9,]+\/[0-9]+)$/";
        foreach ($items as $k => $v) {
            if(empty($v) || !preg_match($pattern, $v)){
                return false;
            }
        }
        return true;
    }

    /**
     * 正则验证服务器key值
     */
    public static function checkServerKey($text)
    {
        if(empty($text) || !preg_match("/^[0-9a-zA-Z]+/", $text)){
            return false;
        }else{
            return true;
        }
    }
}
