本示例使用ThinkPHP6.0+Layui。
使用说明：
1.复制控制器文件(app/admin/controller/TaskLoad.php)到相应的控制器目录中；
2.复制视图文件夹(app/admin/view/task_load/)到相应的视图文件夹中；
3.复制配置文件(config/task_load.php)到config目录中；
4.复制think命令文件(app/command/TaskLoad.php)到项目的自定义命令目录中；
5.以上文件复制过去以后按项目需求做二次修改；