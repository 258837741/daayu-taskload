#!/bin/bash
#判断进程是否存在，如果不存在就启动它
echo "[$1]Start: "$(date +"%Y-%m-%d %H:%M:%S")"" >> /var/log/autotask/taskMonitor.log
limit_num=$2
new_num=0
running=`ps -ef |grep "$1" |grep -v grep -c`
new_num=$[limit_num-running+2]
#echo -e "$limit_num \n $running"
if [ $running -ge $limit_num ]; then
    echo -e "[$1]runing process num is: $running!" >> /var/log/autotask/taskMonitor.log
else
    #运行进程
    echo -e "[$1]run new process num: $new_num" >> /var/log/autotask/taskMonitor.log
    for((i=1;i<=$new_num;i++))
    do
        nohup $1 2>/dev/null
    done
    #nohup php think dianping --op restaurant --piece $1 >> /var/log/autotask/dianping/dianping.sh_$1.log 2>&1 &
fi
echo "[$1]End: "$(date +"%Y-%m-%d %H:%M:%S")"" >> /var/log/autotask/taskMonitor.log
