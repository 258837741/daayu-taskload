<?php
declare (strict_types = 1);

namespace daayu\taskload;

use daayu\taskload\service\TaskPlanService;
use daayu\taskload\service\TaskProjectService;
use daayu\taskload\service\TaskServerService;
use daayu\taskload\service\TaskServerRelateService;
use daayu\taskload\service\TaskServerProjectService;
use think\facade\Db;

/**
 * 计划任务Service层
 */
class TaskLoadService
{
    protected $connection = null;
    protected $taskPlanService = null;
    protected $taskProjectService = null;
    protected $taskServerService = null;
    protected $taskServerRelateService = null;
    protected $taskServerProjectService = null;
    protected $error = null;
    protected $command_type_names = ['', 'think', 'curl'];
    protected $status_names = ['禁用', '启用'];

    public function getError()
    {
        return $this->error;
    }

    /**
     * 构造方法
     */
    public function __construct($connection = null)
    {
        if($connection){
            $this->connection = $connection;
        }
        $this->taskPlanService = new TaskPlanService($this->connection);
        $this->taskProjectService = new TaskProjectService($this->connection);
        $this->taskServerService = new TaskServerService($this->connection);
        $this->taskServerRelateService = new TaskServerRelateService($this->connection);
        $this->taskServerProjectService = new TaskServerProjectService($this->connection);
    }

    private function dealResult($result, $error){
        $this->error = $error;
        return $result;
    }

    /**
     * 任务管理
     */
    public function listTask($args = [])
    {
        return $this->dealResult($this->taskPlanService->list($args), $this->taskPlanService->getError());
    }

    public function getTask($id, $field=null)
    {
        return $this->dealResult($this->taskPlanService->get($id, $field), $this->taskPlanService->getError());
    }

    public function saveTask($data = [])
    {
        return $this->dealResult($this->taskPlanService->save($data), $this->taskPlanService->getError());
    }

    public function deleteTask($id){
        return $this->dealResult($this->taskPlanService->delete($id), $this->taskPlanService->getError());
    }

    /**
     * 项目管理
     */
    public function listProject($args = [])
    {
        return $this->dealResult($this->taskProjectService->list($args), $this->taskProjectService->getError());
    }

    public function listProjectName()
    {
        return $this->dealResult($this->taskProjectService->listName(), $this->taskProjectService->getError());
    }

    public function getProject($id, $field=null)
    {
        return $this->dealResult($this->taskProjectService->get($id, $field), $this->taskProjectService->getError());
    }

    public function saveProject($data = [])
    {
        return $this->dealResult($this->taskProjectService->save($data), $this->taskProjectService->getError());
    }

    public function deleteProject($id){
        return $this->dealResult($this->taskProjectService->delete($id), $this->taskProjectService->getError());
    }

    /**
     * 服务器管理
     */
    public function listServer($args = [])
    {
        return $this->dealResult($this->taskServerService->list($args), $this->taskServerService->getError());
    }

    public function listServerTask($args = [])
    {
        return $this->dealResult($this->taskServerService->listTask($args), $this->taskServerService->getError());
    }

    public function listServerName()
    {
        return $this->dealResult($this->taskServerService->listName(), $this->taskServerService->getError());
    }

    public function getServer($id, $field=null)
    {
        return $this->dealResult($this->taskServerService->get($id, $field), $this->taskServerService->getError());
    }

    public function saveServer($data = [])
    {
        return $this->dealResult($this->taskServerService->save($data), $this->taskServerService->getError());
    }

    public function deleteServer($id){
        return $this->dealResult($this->taskServerService->delete($id), $this->taskServerService->getError());
    }

    public function updateServerTasks($id, $task_ids){
        return $this->dealResult($this->taskServerRelateService->updateServerTasks($id, $task_ids), $this->taskServerRelateService->getError());
    }

    /**
     * 服务器项目管理
     */
    public function listServerProject($args = [])
    {
        return $this->dealResult($this->taskServerProjectService->list($args), $this->taskServerProjectService->getError());
    }

    public function getServerProject($id, $field=null)
    {
        return $this->dealResult($this->taskServerProjectService->get($id, $field), $this->taskServerProjectService->getError());
    }

    public function saveServerProject($data = [])
    {
        return $this->dealResult($this->taskServerProjectService->save($data), $this->taskServerProjectService->getError());
    }

    public function deleteServerProject($args = []){
        return $this->dealResult($this->taskServerProjectService->delete($args), $this->taskServerProjectService->getError());
    }

    /**
     * 服务器任务管理
     */
    public function addRelate($data = [])
    {
        return $this->dealResult($this->taskServerRelateService->add($data), $this->taskServerRelateService->getError());
    }

    public function deleteRelate($data = [])
    {
        return $this->dealResult($this->taskServerRelateService->delete($data), $this->taskServerRelateService->getError());
    }

    public function createDb()
    {
        $this->taskPlanService->createDb();
        $this->taskProjectService->createDb();
        $this->taskServerService->createDb();
        $this->taskServerRelateService->createDb();
    }
}
