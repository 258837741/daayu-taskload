<?php
declare (strict_types = 1);

namespace app\admin\controller;
use app\BaseController;
use think\facade\View;

class Base extends  BaseController
{
    /**
     * @var \think\RequestRequest实例
     */
    protected $request;
    /**
     * @var 控制器中间件
     */
    protected $middleware = [];

    function __construct(\think\App $app = null)
    {
        parent::__construct($app);
        $this->request = request();
    }

    public function assign($name, $value = null){
        View::assign($name, $value);
    }

    public function __call($method, $args)
    {
        return 'error request!';
    }
}
